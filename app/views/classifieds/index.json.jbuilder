json.array!(@classifieds) do |classified|
  json.extract! classified, :id, :AddTitle, :Wing, :FlatNo, :ContactNo, :choose, :Rate, :Description
  json.url classified_url(classified, format: :json)
end
