json.array!(@meetings) do |meeting|
  json.extract! meeting, :id, :phase, :pointsdiscussed, :dateandtime, :venue, :conclusion
  json.url meeting_url(meeting, format: :json)
end
