json.array!(@tenants) do |tenant|
  json.extract! tenant, :id, :username, :firstname, :lastname, :email, :password, :confirmpassword, :mobileno, :dob, :age, :profession, :gender, :peraddress, :nooffamily, :ownerid, :leasestartdate, :leaseenddate, :pancardno
  json.url tenant_url(tenant, format: :json)
end
