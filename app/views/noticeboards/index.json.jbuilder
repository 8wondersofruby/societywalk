json.array!(@noticeboards) do |noticeboard|
  json.extract! noticeboard, :id, :Title, :Description, :Valid_until, :Attach_file
  json.url noticeboard_url(noticeboard, format: :json)
end
