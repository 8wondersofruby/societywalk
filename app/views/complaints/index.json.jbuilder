json.array!(@complaints) do |complaint|
  json.extract! complaint, :id, :nature, :ctype, :category, :description
  json.url complaint_url(complaint, format: :json)
end
