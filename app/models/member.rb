# == Schema Information
#
# Table name: members
#
#  id              :integer          not null, primary key
#  firstname       :string
#  lastname        :string
#  email           :string
#  landlineno      :string
#  mobileno        :string
#  dob             :date
#  gender          :string
#  age             :integer
#  nooffamily      :integer
#  profession      :string
#  pancardno       :string
#  address         :text
#  phasename       :string
#  subphase        :string
#  flatno          :integer
#  flattype        :string
#  flatarea        :float
#  parkingno       :integer
#  nooftwowheeler  :integer
#  nooffourwheeler :integer
#  username        :string
#  password        :string
#  cpassword       :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Member < ActiveRecord::Base
end
