# == Schema Information
#
# Table name: bankaccounts
#
#  id           :integer          not null, primary key
#  acc_no       :integer
#  acc_name     :string
#  bank_name    :string
#  bank_branch  :string
#  bank_add     :text
#  bank_balance :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Bankaccount < ActiveRecord::Base
end
