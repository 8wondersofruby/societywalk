class VenderRegsController < ApplicationController
  before_action :set_vender_reg, only: [:show, :edit, :update, :destroy]

  # GET /vender_regs
  # GET /vender_regs.json
  def index
    @vender_regs = VenderReg.all
  end

  # GET /vender_regs/1
  # GET /vender_regs/1.json
  def show
  end

  # GET /vender_regs/new
  def new
    @vender_reg = VenderReg.new
  end

  # GET /vender_regs/1/edit
  def edit
  end

  # POST /vender_regs
  # POST /vender_regs.json
  def create
    @vender_reg = VenderReg.new(vender_reg_params)

    respond_to do |format|
      if @vender_reg.save
        format.html { redirect_to @vender_reg, notice: 'Vender reg was successfully created.' }
        format.json { render :show, status: :created, location: @vender_reg }
      else
        format.html { render :new }
        format.json { render json: @vender_reg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vender_regs/1
  # PATCH/PUT /vender_regs/1.json
  def update
    respond_to do |format|
      if @vender_reg.update(vender_reg_params)
        format.html { redirect_to @vender_reg, notice: 'Vender reg was successfully updated.' }
        format.json { render :show, status: :ok, location: @vender_reg }
      else
        format.html { render :edit }
        format.json { render json: @vender_reg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vender_regs/1
  # DELETE /vender_regs/1.json
  def destroy
    @vender_reg.destroy
    respond_to do |format|
      format.html { redirect_to vender_regs_url, notice: 'Vender reg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vender_reg
      @vender_reg = VenderReg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vender_reg_params
      params.require(:vender_reg).permit(:Name, :Address, :Contact_no, :DOB, :Gender, :Vendor_inside, :vender_type, :Contracter_Name, :Contracter_N0, :Contracter_period, :Contract_Start_Date, :Pan_No, :Service_Tax, :TDS, :Salary, :Vendor_Outside, :Vendor_Type, :Owner_Name, :Tenant_Name, :Flat_no)
    end
end
