class MemberachivesController < ApplicationController
  before_action :set_memberachive, only: [:show, :edit, :update, :destroy]

  # GET /memberachives
  # GET /memberachives.json
  def index
    @memberachives = Memberachive.all
  end

  # GET /memberachives/1
  # GET /memberachives/1.json
  def show
  end

  # GET /memberachives/new
  def new
    @memberachive = Memberachive.new
  end

  # GET /memberachives/1/edit
  def edit
  end

  # POST /memberachives
  # POST /memberachives.json
  def create
    @memberachive = Memberachive.new(memberachive_params)

    respond_to do |format|
      if @memberachive.save
        format.html { redirect_to @memberachive, notice: 'Memberachive was successfully created.' }
        format.json { render :show, status: :created, location: @memberachive }
      else
        format.html { render :new }
        format.json { render json: @memberachive.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /memberachives/1
  # PATCH/PUT /memberachives/1.json
  def update
    respond_to do |format|
      if @memberachive.update(memberachive_params)
        format.html { redirect_to @memberachive, notice: 'Memberachive was successfully updated.' }
        format.json { render :show, status: :ok, location: @memberachive }
      else
        format.html { render :edit }
        format.json { render json: @memberachive.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /memberachives/1
  # DELETE /memberachives/1.json
  def destroy
    @memberachive.destroy
    respond_to do |format|
      format.html { redirect_to memberachives_url, notice: 'Memberachive was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_memberachive
      @memberachive = Memberachive.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def memberachive_params
      params.require(:memberachive).permit(:FullName, :Achievement, :Description)
    end
end
