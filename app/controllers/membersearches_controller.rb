class MembersearchesController < ApplicationController
  before_action :set_membersearch, only: [:show, :edit, :update, :destroy]

  # GET /membersearches
  # GET /membersearches.json
  def index
    @membersearches = Membersearch.all
  end

  # GET /membersearches/1
  # GET /membersearches/1.json
  def show
  end

  # GET /membersearches/new
  def new
    @membersearch = Membersearch.new
  end

  # GET /membersearches/1/edit
  def edit
  end

  # POST /membersearches
  # POST /membersearches.json
  def create
    @membersearch = Membersearch.new(membersearch_params)

    respond_to do |format|
      if @membersearch.save
        format.html { redirect_to @membersearch, notice: 'Membersearch was successfully created.' }
        format.json { render :show, status: :created, location: @membersearch }
      else
        format.html { render :new }
        format.json { render json: @membersearch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /membersearches/1
  # PATCH/PUT /membersearches/1.json
  def update
    respond_to do |format|
      if @membersearch.update(membersearch_params)
        format.html { redirect_to @membersearch, notice: 'Membersearch was successfully updated.' }
        format.json { render :show, status: :ok, location: @membersearch }
      else
        format.html { render :edit }
        format.json { render json: @membersearch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /membersearches/1
  # DELETE /membersearches/1.json
  def destroy
    @membersearch.destroy
    respond_to do |format|
      format.html { redirect_to membersearches_url, notice: 'Membersearch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_membersearch
      @membersearch = Membersearch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membersearch_params
      params.require(:membersearch).permit(:Name)
    end
end
