class CommittmemregsController < ApplicationController
  before_action :set_committmemreg, only: [:show, :edit, :update, :destroy]

  # GET /committmemregs
  # GET /committmemregs.json
  def index
    @committmemregs = Committmemreg.all
  end

  # GET /committmemregs/1
  # GET /committmemregs/1.json
  def show
  end

  # GET /committmemregs/new
  def new
    @committmemreg = Committmemreg.new
  end

  # GET /committmemregs/1/edit
  def edit
  end

  # POST /committmemregs
  # POST /committmemregs.json
  def create
    @committmemreg = Committmemreg.new(committmemreg_params)

    respond_to do |format|
      if @committmemreg.save
        format.html { redirect_to @committmemreg, notice: 'Committmemreg was successfully created.' }
        format.json { render :show, status: :created, location: @committmemreg }
      else
        format.html { render :new }
        format.json { render json: @committmemreg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /committmemregs/1
  # PATCH/PUT /committmemregs/1.json
  def update
    respond_to do |format|
      if @committmemreg.update(committmemreg_params)
        format.html { redirect_to @committmemreg, notice: 'Committmemreg was successfully updated.' }
        format.json { render :show, status: :ok, location: @committmemreg }
      else
        format.html { render :edit }
        format.json { render json: @committmemreg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /committmemregs/1
  # DELETE /committmemregs/1.json
  def destroy
    @committmemreg.destroy
    respond_to do |format|
      format.html { redirect_to committmemregs_url, notice: 'Committmemreg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_committmemreg
      @committmemreg = Committmemreg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def committmemreg_params
      params.require(:committmemreg).permit(:Full_Name, :Gender, :Role, :Email, :Phase_No, :Responsible_For, :Start_Date, :Duration)
    end
end
