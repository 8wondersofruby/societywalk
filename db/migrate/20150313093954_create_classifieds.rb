class CreateClassifieds < ActiveRecord::Migration
  def change
    create_table :classifieds do |t|
      t.string :AddTitle
      t.string :Wing
      t.integer :FlatNo
      t.string :ContactNo
      t.string :choose
      t.integer :Rate
      t.text :Description

      t.timestamps null: false
    end
  end
end
