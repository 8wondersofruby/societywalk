class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.string :Full_name
      t.date :Date
      t.string :mobile_no
      t.time :Time_in
      t.time :Time_out
      t.boolean :visitors
      t.string :Owner_name
      t.integer :Phase_no
      t.integer :Flat_no
      t.string :Vendor
      t.string :Vendor_type
      t.integer :Phase_no
      t.boolean :Morning
      t.boolean :Evening

      t.timestamps null: false
    end
  end
end
