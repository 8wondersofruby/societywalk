class CreateVenderRegs < ActiveRecord::Migration
  def change
    create_table :vender_regs do |t|
      t.string :Name
      t.text :Address
      t.string :Contact_no
      t.date :Dath_of_Birth
      t.string :Gender
      t.boolean :Vendor_inside
      t.string :vender_type
      t.string :Contracter_Name
      t.string :Contracter_N0
      t.integer :Contracter_period
      t.date :Contract_Start_Date
      t.string :Pan_No
      t.float :Service_Tax
      t.float :TDS
      t.float :Salary
      t.boolean :Vendor_Outside
      t.string :Vendor_Type
      t.string :Owner_Name
      t.string :Tenant_Name
      t.integer :Flat_no

      t.timestamps null: false
    end
  end
end
