class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :landlineno
      t.string :mobileno
      t.date :dob
      t.string :gender
      t.integer :age
      t.integer :nooffamily
      t.string :profession
      t.string :pancardno
      t.text :address
      t.string :phasename
      t.string :subphase
      t.integer :flatno
      t.string :flattype
      t.float :flatarea
      t.integer :parkingno
      t.integer :nooftwowheeler
      t.integer :nooffourwheeler
      t.string :username
      t.string :password
      t.string :cpassword
     

      t.timestamps null: false
    end
  end
end
