class CreateNoticeboards < ActiveRecord::Migration
  def change
    create_table :noticeboards do |t|
      t.string :Title
      t.text :Description
      t.date :Valid_until
      t.string :Attach_file

      t.timestamps null: false
    end
  end
end
