class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.string :username
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :password
      t.string :confirmpassword
      t.string :mobileno
      t.date :dob
      t.integer :age
      t.string :profession
      t.string :gender
      t.text :peraddress
      t.integer :nooffamily
      t.integer :ownerid
      t.date :leasestartdate
      t.date :leaseenddate
      t.string :pancardno

      t.timestamps null: false
    end
  end
end
