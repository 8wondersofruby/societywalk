class CreateMemberachives < ActiveRecord::Migration
  def change
    create_table :memberachives do |t|
      t.string :FullName
      t.string :Achievement
      t.text :Description

      t.timestamps null: false
    end
  end
end
