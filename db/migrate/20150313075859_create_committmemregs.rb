class CreateCommittmemregs < ActiveRecord::Migration
  def change
    create_table :committmemregs do |t|
      t.string :Full_Name
      t.string :Gender
      t.string :Role
      t.string :Email
      t.integer :Phase_No
      t.text :Responsible_For
      t.date :Start_Date
      t.integer :Duration

      t.timestamps null: false
    end
  end
end
