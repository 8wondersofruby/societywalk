class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :phase
      t.text :pointsdiscussed
      t.datetime :dateandtime
      t.string :venue
      t.text :conclusion

      t.timestamps null: false
    end
  end
end
