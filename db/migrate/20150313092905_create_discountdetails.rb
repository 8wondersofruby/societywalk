class CreateDiscountdetails < ActiveRecord::Migration
  def change
    create_table :discountdetails do |t|
      t.text :discount_desc
      t.float :disfix_amt
      t.float :dis_percentages

      t.timestamps null: false
    end
  end
end
