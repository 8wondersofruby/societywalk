class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.string :nature
      t.string :ctype
      t.string :category
      t.text :description

      t.timestamps null: false
    end
  end
end
