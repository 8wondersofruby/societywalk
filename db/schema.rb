# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.


ActiveRecord::Schema.define(version: 20150313120834) do

  create_table "complaints", force: :cascade do |t|
    t.string   "nature"
    t.string   "ctype"
    t.string   "category"
    t.text     "description"

ActiveRecord::Schema.define(version: 20150313114111) do

  create_table "attendances", force: :cascade do |t|
    t.string   "Full_name"
    t.date     "Date"
    t.integer  "mobile_no"
    t.time     "Time_in"
    t.time     "Time_out"
    t.string   "visitors"
    t.string   "Owner_name"
    t.integer  "Phase_no"
    t.integer  "Flat_no"
    t.string   "Vendor"
    t.string   "Vendor_type"
    t.boolean  "Morning"
    t.boolean  "Evening"

ActiveRecord::Schema.define(version: 20150313105534) do

  create_table "bankaccounts", force: :cascade do |t|
    t.integer  "acc_no"
    t.string   "acc_name"
    t.string   "bank_name"
    t.string   "bank_branch"
    t.text     "bank_add"
    t.float    "bank_balance"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "classifieds", force: :cascade do |t|
    t.string   "AddTitle"
    t.string   "Wing"
    t.integer  "FlatNo"
    t.string   "ContactNo"
    t.string   "choose"
    t.integer  "Rate"
    t.text     "Description"

    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end


  create_table "meetings", force: :cascade do |t|
    t.string   "phase"
    t.text     "pointsdiscussed"
    t.datetime "dateandtime"
    t.string   "venue"
    t.text     "conclusion"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "committmemregs", force: :cascade do |t|
    t.string   "Full_Name"
    t.string   "Gender"
    t.string   "Role"
    t.string   "Email"
    t.integer  "Phase_No"
    t.text     "Responsible_For"
    t.date     "Start_Date"
    t.integer  "Duration"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "discountdetails", force: :cascade do |t|
    t.text     "discount_desc"
    t.float    "disfix_amt"
    t.float    "dis_percentages"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "instant_member_searches", force: :cascade do |t|
    t.string   "Name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberachives", force: :cascade do |t|
    t.string   "FullName"
    t.string   "Achievement"
    t.text     "Description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end


  create_table "members", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "landlineno"
    t.string   "mobileno"
    t.date     "dob"
    t.string   "gender"
    t.integer  "age"
    t.integer  "nooffamily"
    t.string   "profession"
    t.string   "pancardno"
    t.text     "address"
    t.string   "phasename"
    t.string   "subphase"
    t.integer  "flatno"
    t.string   "flattype"
    t.float    "flatarea"
    t.integer  "parkingno"
    t.integer  "nooftwowheeler"
    t.integer  "nooffourwheeler"
    t.string   "username"
    t.string   "password"
    t.string   "cpassword"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end


  create_table "tenants", force: :cascade do |t|
    t.string   "username"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "password"
    t.string   "confirmpassword"
    t.string   "mobileno"
    t.date     "dob"
    t.integer  "age"
    t.string   "profession"
    t.string   "gender"
    t.text     "peraddress"
    t.integer  "nooffamily"
    t.integer  "ownerid"
    t.date     "leasestartdate"
    t.date     "leaseenddate"
    t.string   "pancardno"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false

  create_table "noticeboards", force: :cascade do |t|
    t.string   "Title"
    t.text     "Description"
    t.date     "Valid_until"
    t.string   "Attach_file"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "vender_regs", force: :cascade do |t|
    t.string   "Name"
    t.text     "Address"
    t.string   "Contact_no"
    t.date     "DOB"
    t.string   "Gender"
    t.boolean  "Vendor_inside"
    t.string   "vender_type"
    t.string   "Contracter_Name"
    t.string   "Contracter_N0"
    t.integer  "Contracter_period"
    t.date     "Contract_Start_Date"
    t.string   "Pan_No"
    t.float    "Service_Tax"
    t.float    "TDS"
    t.float    "Salary"
    t.boolean  "Vendor_Outside"
    t.string   "Vendor_Type"
    t.string   "Owner_Name"
    t.string   "Tenant_Name"
    t.integer  "Flat_no"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false

  create_table "membersearches", force: :cascade do |t|
    t.string   "Name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false

  end

end
