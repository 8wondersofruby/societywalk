require 'test_helper'

class MembersearchesControllerTest < ActionController::TestCase
  setup do
    @membersearch = membersearches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:membersearches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create membersearch" do
    assert_difference('Membersearch.count') do
      post :create, membersearch: { Name: @membersearch.Name }
    end

    assert_redirected_to membersearch_path(assigns(:membersearch))
  end

  test "should show membersearch" do
    get :show, id: @membersearch
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @membersearch
    assert_response :success
  end

  test "should update membersearch" do
    patch :update, id: @membersearch, membersearch: { Name: @membersearch.Name }
    assert_redirected_to membersearch_path(assigns(:membersearch))
  end

  test "should destroy membersearch" do
    assert_difference('Membersearch.count', -1) do
      delete :destroy, id: @membersearch
    end

    assert_redirected_to membersearches_path
  end
end
