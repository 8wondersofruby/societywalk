require 'test_helper'

class CommittmemregsControllerTest < ActionController::TestCase
  setup do
    @committmemreg = committmemregs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:committmemregs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create committmemreg" do
    assert_difference('Committmemreg.count') do
      post :create, committmemreg: { Duration: @committmemreg.Duration, Email: @committmemreg.Email, Full_Name: @committmemreg.Full_Name, Gender: @committmemreg.Gender, Phase_No: @committmemreg.Phase_No, Responsible_For: @committmemreg.Responsible_For, Role: @committmemreg.Role, Start_Date: @committmemreg.Start_Date }
    end

    assert_redirected_to committmemreg_path(assigns(:committmemreg))
  end

  test "should show committmemreg" do
    get :show, id: @committmemreg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @committmemreg
    assert_response :success
  end

  test "should update committmemreg" do
    patch :update, id: @committmemreg, committmemreg: { Duration: @committmemreg.Duration, Email: @committmemreg.Email, Full_Name: @committmemreg.Full_Name, Gender: @committmemreg.Gender, Phase_No: @committmemreg.Phase_No, Responsible_For: @committmemreg.Responsible_For, Role: @committmemreg.Role, Start_Date: @committmemreg.Start_Date }
    assert_redirected_to committmemreg_path(assigns(:committmemreg))
  end

  test "should destroy committmemreg" do
    assert_difference('Committmemreg.count', -1) do
      delete :destroy, id: @committmemreg
    end

    assert_redirected_to committmemregs_path
  end
end
