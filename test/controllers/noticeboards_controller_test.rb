require 'test_helper'

class NoticeboardsControllerTest < ActionController::TestCase
  setup do
    @noticeboard = noticeboards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:noticeboards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create noticeboard" do
    assert_difference('Noticeboard.count') do
      post :create, noticeboard: { Attach_file: @noticeboard.Attach_file, Description: @noticeboard.Description, Title: @noticeboard.Title, Valid_until: @noticeboard.Valid_until }
    end

    assert_redirected_to noticeboard_path(assigns(:noticeboard))
  end

  test "should show noticeboard" do
    get :show, id: @noticeboard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @noticeboard
    assert_response :success
  end

  test "should update noticeboard" do
    patch :update, id: @noticeboard, noticeboard: { Attach_file: @noticeboard.Attach_file, Description: @noticeboard.Description, Title: @noticeboard.Title, Valid_until: @noticeboard.Valid_until }
    assert_redirected_to noticeboard_path(assigns(:noticeboard))
  end

  test "should destroy noticeboard" do
    assert_difference('Noticeboard.count', -1) do
      delete :destroy, id: @noticeboard
    end

    assert_redirected_to noticeboards_path
  end
end
