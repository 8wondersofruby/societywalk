require 'test_helper'

class MemberachivesControllerTest < ActionController::TestCase
  setup do
    @memberachive = memberachives(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:memberachives)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create memberachive" do
    assert_difference('Memberachive.count') do
      post :create, memberachive: { Achievement: @memberachive.Achievement, Description: @memberachive.Description, FullName: @memberachive.FullName }
    end

    assert_redirected_to memberachive_path(assigns(:memberachive))
  end

  test "should show memberachive" do
    get :show, id: @memberachive
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @memberachive
    assert_response :success
  end

  test "should update memberachive" do
    patch :update, id: @memberachive, memberachive: { Achievement: @memberachive.Achievement, Description: @memberachive.Description, FullName: @memberachive.FullName }
    assert_redirected_to memberachive_path(assigns(:memberachive))
  end

  test "should destroy memberachive" do
    assert_difference('Memberachive.count', -1) do
      delete :destroy, id: @memberachive
    end

    assert_redirected_to memberachives_path
  end
end
