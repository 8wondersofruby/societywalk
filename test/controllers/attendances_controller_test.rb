require 'test_helper'

class AttendancesControllerTest < ActionController::TestCase
  setup do
    @attendance = attendances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attendances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attendance" do
    assert_difference('Attendance.count') do
      post :create, attendance: { Date: @attendance.Date, Evening: @attendance.Evening, Flat_no: @attendance.Flat_no, Full_name: @attendance.Full_name, Morning: @attendance.Morning, Owner_name: @attendance.Owner_name, Phase_no: @attendance.Phase_no, Phase_no: @attendance.Phase_no, Time_in: @attendance.Time_in, Time_out: @attendance.Time_out, Vendor: @attendance.Vendor, Vendor_type: @attendance.Vendor_type, mobile_no: @attendance.mobile_no, visitors: @attendance.visitors }
    end

    assert_redirected_to attendance_path(assigns(:attendance))
  end

  test "should show attendance" do
    get :show, id: @attendance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attendance
    assert_response :success
  end

  test "should update attendance" do
    patch :update, id: @attendance, attendance: { Date: @attendance.Date, Evening: @attendance.Evening, Flat_no: @attendance.Flat_no, Full_name: @attendance.Full_name, Morning: @attendance.Morning, Owner_name: @attendance.Owner_name, Phase_no: @attendance.Phase_no, Phase_no: @attendance.Phase_no, Time_in: @attendance.Time_in, Time_out: @attendance.Time_out, Vendor: @attendance.Vendor, Vendor_type: @attendance.Vendor_type, mobile_no: @attendance.mobile_no, visitors: @attendance.visitors }
    assert_redirected_to attendance_path(assigns(:attendance))
  end

  test "should destroy attendance" do
    assert_difference('Attendance.count', -1) do
      delete :destroy, id: @attendance
    end

    assert_redirected_to attendances_path
  end
end
