require 'test_helper'

class VenderRegsControllerTest < ActionController::TestCase
  setup do
    @vender_reg = vender_regs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vender_regs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vender_reg" do
    assert_difference('VenderReg.count') do
      post :create, vender_reg: { Address: @vender_reg.Address, Contact_no: @vender_reg.Contact_no, Contract_Start_Date: @vender_reg.Contract_Start_Date, Contracter_N0: @vender_reg.Contracter_N0, Contracter_Name: @vender_reg.Contracter_Name, Contracter_period: @vender_reg.Contracter_period, DOB: @vender_reg.DOB, Flat_no: @vender_reg.Flat_no, Gender: @vender_reg.Gender, Name: @vender_reg.Name, Owner_Name: @vender_reg.Owner_Name, Pan_No: @vender_reg.Pan_No, Salary: @vender_reg.Salary, Service_Tax: @vender_reg.Service_Tax, TDS: @vender_reg.TDS, Tenant_Name: @vender_reg.Tenant_Name, Vendor_Outside: @vender_reg.Vendor_Outside, Vendor_Type: @vender_reg.Vendor_Type, Vendor_inside: @vender_reg.Vendor_inside, vender_type: @vender_reg.vender_type }
    end

    assert_redirected_to vender_reg_path(assigns(:vender_reg))
  end

  test "should show vender_reg" do
    get :show, id: @vender_reg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vender_reg
    assert_response :success
  end

  test "should update vender_reg" do
    patch :update, id: @vender_reg, vender_reg: { Address: @vender_reg.Address, Contact_no: @vender_reg.Contact_no, Contract_Start_Date: @vender_reg.Contract_Start_Date, Contracter_N0: @vender_reg.Contracter_N0, Contracter_Name: @vender_reg.Contracter_Name, Contracter_period: @vender_reg.Contracter_period, DOB: @vender_reg.DOB, Flat_no: @vender_reg.Flat_no, Gender: @vender_reg.Gender, Name: @vender_reg.Name, Owner_Name: @vender_reg.Owner_Name, Pan_No: @vender_reg.Pan_No, Salary: @vender_reg.Salary, Service_Tax: @vender_reg.Service_Tax, TDS: @vender_reg.TDS, Tenant_Name: @vender_reg.Tenant_Name, Vendor_Outside: @vender_reg.Vendor_Outside, Vendor_Type: @vender_reg.Vendor_Type, Vendor_inside: @vender_reg.Vendor_inside, vender_type: @vender_reg.vender_type }
    assert_redirected_to vender_reg_path(assigns(:vender_reg))
  end

  test "should destroy vender_reg" do
    assert_difference('VenderReg.count', -1) do
      delete :destroy, id: @vender_reg
    end

    assert_redirected_to vender_regs_path
  end
end
