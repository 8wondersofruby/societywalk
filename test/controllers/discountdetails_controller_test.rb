require 'test_helper'

class DiscountdetailsControllerTest < ActionController::TestCase
  setup do
    @discountdetail = discountdetails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:discountdetails)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create discountdetail" do
    assert_difference('Discountdetail.count') do
      post :create, discountdetail: { dis_percentages: @discountdetail.dis_percentages, discount_desc: @discountdetail.discount_desc, disfix_amt: @discountdetail.disfix_amt }
    end

    assert_redirected_to discountdetail_path(assigns(:discountdetail))
  end

  test "should show discountdetail" do
    get :show, id: @discountdetail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @discountdetail
    assert_response :success
  end

  test "should update discountdetail" do
    patch :update, id: @discountdetail, discountdetail: { dis_percentages: @discountdetail.dis_percentages, discount_desc: @discountdetail.discount_desc, disfix_amt: @discountdetail.disfix_amt }
    assert_redirected_to discountdetail_path(assigns(:discountdetail))
  end

  test "should destroy discountdetail" do
    assert_difference('Discountdetail.count', -1) do
      delete :destroy, id: @discountdetail
    end

    assert_redirected_to discountdetails_path
  end
end
