require 'test_helper'

class MembersControllerTest < ActionController::TestCase
  setup do
    @member = members(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:members)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create member" do
    assert_difference('Member.count') do
      post :create, member: { address: @member.address, age: @member.age, cpassword: @member.cpassword, dob: @member.dob, email: @member.email, firstname: @member.firstname, flatarea: @member.flatarea, flatno: @member.flatno, flattype: @member.flattype, gender: @member.gender, landlineno: @member.landlineno, lastname: @member.lastname, mobileno: @member.mobileno, nooffamily: @member.nooffamily, nooffourwheeler: @member.nooffourwheeler, nooftwowheeler: @member.nooftwowheeler, pancardno: @member.pancardno, parkingno: @member.parkingno, password: @member.password, phasename: @member.phasename, profession: @member.profession, subphase: @member.subphase, username: @member.username }
    end

    assert_redirected_to member_path(assigns(:member))
  end

  test "should show member" do
    get :show, id: @member
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @member
    assert_response :success
  end

  test "should update member" do
    patch :update, id: @member, member: { address: @member.address, age: @member.age, cpassword: @member.cpassword, dob: @member.dob, email: @member.email, firstname: @member.firstname, flatarea: @member.flatarea, flatno: @member.flatno, flattype: @member.flattype, gender: @member.gender, landlineno: @member.landlineno, lastname: @member.lastname, mobileno: @member.mobileno, nooffamily: @member.nooffamily, nooffourwheeler: @member.nooffourwheeler, nooftwowheeler: @member.nooftwowheeler, pancardno: @member.pancardno, parkingno: @member.parkingno, password: @member.password, phasename: @member.phasename, profession: @member.profession, subphase: @member.subphase, username: @member.username }
    assert_redirected_to member_path(assigns(:member))
  end

  test "should destroy member" do
    assert_difference('Member.count', -1) do
      delete :destroy, id: @member
    end

    assert_redirected_to members_path
  end
end
