require 'test_helper'

class TenantsControllerTest < ActionController::TestCase
  setup do
    @tenant = tenants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tenants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tenant" do
    assert_difference('Tenant.count') do
      post :create, tenant: { age: @tenant.age, confirmpassword: @tenant.confirmpassword, dob: @tenant.dob, email: @tenant.email, firstname: @tenant.firstname, gender: @tenant.gender, lastname: @tenant.lastname, leaseenddate: @tenant.leaseenddate, leasestartdate: @tenant.leasestartdate, mobileno: @tenant.mobileno, nooffamily: @tenant.nooffamily, ownerid: @tenant.ownerid, pancardno: @tenant.pancardno, password: @tenant.password, peraddress: @tenant.peraddress, profession: @tenant.profession, username: @tenant.username }
    end

    assert_redirected_to tenant_path(assigns(:tenant))
  end

  test "should show tenant" do
    get :show, id: @tenant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tenant
    assert_response :success
  end

  test "should update tenant" do
    patch :update, id: @tenant, tenant: { age: @tenant.age, confirmpassword: @tenant.confirmpassword, dob: @tenant.dob, email: @tenant.email, firstname: @tenant.firstname, gender: @tenant.gender, lastname: @tenant.lastname, leaseenddate: @tenant.leaseenddate, leasestartdate: @tenant.leasestartdate, mobileno: @tenant.mobileno, nooffamily: @tenant.nooffamily, ownerid: @tenant.ownerid, pancardno: @tenant.pancardno, password: @tenant.password, peraddress: @tenant.peraddress, profession: @tenant.profession, username: @tenant.username }
    assert_redirected_to tenant_path(assigns(:tenant))
  end

  test "should destroy tenant" do
    assert_difference('Tenant.count', -1) do
      delete :destroy, id: @tenant
    end

    assert_redirected_to tenants_path
  end
end
